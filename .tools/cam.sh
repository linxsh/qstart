#!/bin/bash

# Set the directory to save photos
directory="/data/data/com.termux/files/home/storage/dcim/tcam/"
# Set the file name for the photo
file_name="photo_$(date +%Y%m%d_%H%M%S).jpg"
full_path="$directory$file_name"

# Function to show the help message
show_help() {
    echo "Usage: cam.sh [-x | -w | -p | -r | -n | -i | -v | -m | -f | -s | -o | -g] [-c] [-h]"
    echo "Applies an effect to the photo taken using Termux."
    echo ""
    echo "Options:"
    echo "  -x    Apply X PRO II effect"
    echo "  -w    Apply Willow effect"
    echo "  -p    Apply Poprocket effect"
    echo "  -r    Apply Walden effect"
    echo "  -n    Apply Nashville effect"
    echo "  -i    Apply Inkwell effect"
    echo "  -v    Apply Valencia effect"
    echo "  -m    Apply Mayfair effect"
    echo "  -f    Apply Amaro effect"
    echo "  -s    Apply Sierra effect"
    echo "  -o    Apply Pokémon effect (pixelated style)"
    echo "  -g    Apply Game Boy Color (GBC) effect"
    echo "  -c    Remove colors from the photo"
    echo "  -h    Show this help message"
    echo ""
}

# Check if no command-line argument is provided
if [ $# -eq 0 ]; then
    effect="mosaic"
fi

# Parse command-line arguments
while getopts ":xwprnivmfsogch" option; do
    case ${option} in
        x) effect="xpro" ;;
        w) effect="willow" ;;
        p) effect="poprocket" ;;
        r) effect="walden" ;;
        n) effect="nashville" ;;
        i) effect="inkwell" ;;
        v) effect="valencia" ;;
        m) effect="mayfair" ;;
        f) effect="amaro" ;;
        s) effect="sierra" ;;
        o) effect="pokemon" ;;
        g) effect="gbc" ;;
        c) remove_colors=true ;;
        h)
            show_help
            exit 0
            ;;
        \?)
            show_help
            exit 1
            ;;
    esac
done

# Take a photo using Termux-API with default settings
termux-camera-photo -c 0 $full_path

# Confirm if the photo was taken successfully
if [ $? -eq 0 ]; then
    echo "Photo taken successfully: $full_path"
    
    # Reduce the resolution of the photo
    convert $full_path -resize 800x600\> $full_path
    
    # Apply mosaic effect with original colors in each square
    if [ "$remove_colors" = true ]; then
        convert $full_path -scale 8% -scale 1250% -quantize RGB -colors 16 $full_path
    else
        convert $full_path -scale 8% -scale 1250% $full_path
    fi
    
    # Apply the chosen effect
    case $effect in
        xpro)
            # Apply X PRO II effect
            convert $full_path -channel R -level 10%,100% -channel B -level 10%,100% $full_path
            ;;
        willow)
            # Apply Willow effect
            convert $full_path -colorspace Gray -sepia-tone 80% -contrast-stretch 0% -colorspace sRGB $full_path
            ;;
        poprocket)
            # Apply Poprocket effect
            convert $full_path -contrast -modulate 100,120,100 $full_path
            ;;
        walden)
            # Apply Walden effect
            convert $full_path -contrast -modulate 120,100,100 -sepia-tone 80% $full_path
            ;;
        nashville)
            # Apply Nashville effect
            convert $full_path -modulate 100,90,100 -colorize 10,0,0 $full_path
            ;;
        inkwell)
            # Apply Inkwell effect
            convert $full_path -colorspace Gray -sigmoidal-contrast 10,50% $full_path
            ;;
        valencia)
            # Apply Valencia effect
            convert $full_path -modulate 120,90,100 -fill "#d29898" -tint 100 $full_path
            ;;
        mayfair)
            # Apply Mayfair effect
            convert $full_path -modulate 130,100,100 -fill "#d8d0c8" -tint 100 $full_path
            ;;
        amaro)
            # Apply Amaro effect
            convert $full_path -modulate 110,100,100 -fill "#3e4415" -tint 100 $full_path
            ;;
        sierra)
            # Apply Sierra effect
            convert $full_path -modulate 120,100,100 -fill "#5940a8" -tint 100 $full_path
            ;;
        pokemon)
            # Apply Pokémon effect (pixelated style)
            convert $full_path -scale 200% -scale 50% $full_path
            ;;
        gbc)
            # Apply Game Boy Color (GBC) effect
            convert $full_path +dither -colors 4 -define png:color-type=3 -scale 128x128! $full_path
            ;;
        mosaic)
            echo "Mosaic effect applied with original colors in each square"
            ;;
    esac
    
    # Rename the final file randomly
    new_name="photo_$(date +%s%N | md5sum | head -c 8).jpg"
    new_path="$directory$new_name"
    mv $full_path $new_path
    
    echo "Effect applied. Photo saved as: $new_path"
else
    echo "Error taking the photo"
fi
