#!/bin/bash

# Set the directory to save photos
directory="/data/data/com.termux/files/home/storage/dcim/tcam/"
# Set the file name for the photo
file_name="photo_$(date +%Y%m%d_%H%M%S).jpg"
full_path="$directory$file_name"

# Function to show the help message
show_help() {
    echo "Usage: cam.sh [-n] [-g] [-s] [-h]"
    echo "Takes a photo using Termux-API and applies effects if specified."
    echo ""
    echo "Options:"
    echo "  -n    Apply secondary effect (optional)"
    echo "  -g    Apply Game Boy Color (GBC) effect (optional)"
    echo "  -s    Apply grayscale effect (optional)"
    echo "  -h    Show this help message"
    echo ""
}

# Check if no command-line argument is provided
if [ $# -eq 0 ]; then
    effect="default"
else
    # Parse command-line arguments
    while getopts ":ngsh" option; do
        case ${option} in
            n) effect="secondary" ;;
            g) effect="gbc" ;;
            s) effect="grayscale" ;;
            h)
                show_help
                exit 0
                ;;
            \?)
                show_help
                exit 1
                ;;
        esac
    done
fi

# Take a photo using Termux-API with default settings
termux-camera-photo -c 0 $full_path

# Confirm if the photo was taken successfully
if [ $? -eq 0 ]; then
    echo "Photo taken successfully: $full_path"
    # Apply the chosen effect
    case $effect in
        default)
            # Apply default effect
            convert $full_path -scale 5% -scale 1250% -quantize RGB -colors 16 $full_path
            ;;
        secondary)
            # Apply secondary effect
            convert $full_path -scale 5% -scale 1250% -colors 16 $full_path
            ;;
        gbc)
            # Apply Game Boy Color (GBC) effect
            convert $full_path +dither -colors 4 -define png:color-type=3 -scale 128x128! $full_path
            ;;
        grayscale)
            # Apply grayscale effect
            convert $full_path -colorspace Gray -scale 5% -scale 1250% $full_path
            ;;
    esac

    # Rename the final file randomly
    new_name="photo_$(date +%s%N | md5sum | head -c 8).jpg"
    new_path="$directory$new_name"
    mv $full_path $new_path

    echo "Effect applied. Photo saved as: $new_path"
else
    echo "Error taking the photo"
fi
