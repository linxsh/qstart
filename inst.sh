#!/bin/bash

termux-setup-storage

pkg update -y && pkg upgrade -y

while true; do
    clear
    echo "Select:"
    echo "1. Customize shell"
    echo "2. Uninstall customization"
    echo "3. Basic packages"
    echo "0. Exit"
    read -p "Enter the number: " option

case $option in
        1)
            echo "Customizing shell"
            rm -rf ~/.bashrc
            cp -f .bashrc ~/
            mkdir ~/.garbage
            cp -r .cfgs ~/
            cp -f fonts/font.ttf ~/.termux/
            pkg install lsd -y
            truncate -s 0 /data/data/com.termux/files/usr/etc/motd
            echo "successfully installed!"
            ;;

        2)
            read -p "Proceed? 'y/n': " resposta
            if [ "$resposta" == "y" ]; then
                echo "Uninstalling!"
                bash ~/.cfgs/uninst.sh
                echo "successfully uninstalled!"
            fi
            ;;
        3)
            echo "Installing basic packages..."
            pkg install nmap -y
            pkg install python -y
            pkg install netcat-openbsd -y
            pkg install net-tools -y
            pkg install wget -y
            pkg install android-tools -y
            pkg install curl -y
            pkg install unzip -y
            pkg install zip -y
            ;;
        0)
            echo "Exiting..."
            clear
            exit 0
            ;;
        *)
            echo "Invalid option."
            ;;
    esac

    read -n1 -r -p "Press any key to continue..."
done

echo "Done."
