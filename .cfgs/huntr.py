import os
import argparse
import fnmatch

def search_files(term, option, search_symlinks):
    results = []

    for root, dirs, files in os.walk('.'):
        for name in dirs:
            full_path = os.path.join(root, name)
            if (option == 'd' or option == 'a') and term in name:
                results.append(full_path)
        for name in files:
            full_path = os.path.join(root, name)
            if (option == 'f' or option == 'a') and term in name:
                results.append(full_path)
            elif option == 'i':
                try:
                    with open(full_path, 'r', encoding='utf-8') as f:
                        content = f.read()
                        if term in content:
                            results.append(full_path)
                except UnicodeDecodeError:
                    continue

    if search_symlinks:
        symlink_results = []

        for root, dirs, files in os.walk('.', followlinks=True):
            for name in dirs:
                full_path = os.path.join(root, name)
                if term in name:
                    symlink_results.append(full_path)
            for name in files:
                full_path = os.path.join(root, name)
                if term in name:
                    symlink_results.append(full_path)

        results += symlink_results

    return results

parser = argparse.ArgumentParser(description='File and directory searcher.')
parser.add_argument('search_term', type=str, help='Term to search')
parser.add_argument('-i', action='store_true', help='Search inside files')
parser.add_argument('-f', action='store_true', help='Search only files')
parser.add_argument('-d', action='store_true', help='Search only directories')
parser.add_argument('-a', action='store_true', help='Search in files, directories, and inside files')
parser.add_argument('-s', '--symlinks', action='store_true', help='Include symbolic links in the search')

args = parser.parse_args()

option = ''
if args.i:
    option = 'i'
elif args.f:
    option = 'f'
elif args.d:
    option = 'd'
elif args.a:
    option = 'a'
else:
    parser.error('Select a search option (-i, -f, -d, -a)')

search_symlinks = args.symlinks

results = search_files(args.search_term, option, search_symlinks)

if results:
    print("Files or directories found:")
    for result in results:
        print(os.path.abspath(result))
else:
    print("No files or directories found with the search term.")
